"""
BSD 3-Clause License

Copyright (c) 2017, Linus
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""



import ctypes as ct
import datetime
import os
module_dir = os.path.dirname(__file__)
import sys
path_to_xll = os.path.join(module_dir, r'cds_v1.8.2.xll')
sys.path.append(module_dir)
ct.cdll.LoadLibrary(os.path.join(module_dir, "XLCALL32.DLL"))
cdsdll = ct.CDLL(path_to_xll)
"""A TDate is a long representing the number of days since January 1, 1601"""
tdate_base = datetime.date(1601, 1, 1).toordinal()
TDate = ct.c_long
TBoolean = ct.c_int
SUCCESS = 1
FAILURE = -1
jpm_dcc = {'ACT/365': 1, 'ACT/365F': 2, 'ACT/360': 3, 'B30/360': 4,
           '30/360': 4,#From ISDA rates files
           'B30E/360': 5, 'ACT/ACT': 1, 'EFFECTIVE_RATE': 8, 'DCC_LAST': 8}
jpm_interval_to_freq = {'1M': 12, '3M': 4, '6M': 2}


"""ctypes class definitions"""
class OPER(ct.Structure):
    _fields_ = [("num", ct.c_double), ("type", ct.c_int)]


class TDateInterval(ct.Structure):
    """
    lib/include/isda/cdate.h:41
    typedef struct
    {
        int prd;        /* number of periods from offset date                  */
        char prd_typ;   /* type of periods                                     */
                        /* D - day; M - month; W - week                        */
                        /* Q - 3 months; S - 6 months                          */
                        /* A - 12 months; Y - 12 months                        */
                        /* I - quarterly IMM period                            */
                        /* F - flexible end of month                           */
                        /* E - end of month                                    */
                        /* J - monthly IMM period                              */
                        /* K - quarterly Australian futures period             */
                        /* L - quarterly New Zealand (kiwi) futures period     */
                        /* T - equity derivatives expiry - 3rd Friday monthly  */
                               the date at that location is an offset */
    } TDateInterval;
    """
    _fields_ = [("prd", ct.c_int),
                ("prd_typ", ct.c_char),
                ("flag", ct.c_int)]


class TStubMethod(ct.Structure):
    """
    lib/include/isda/stub.h:54
    typedef struct _TStubMethod
    {
        TBoolean stubAtEnd;
        TBoolean longStub;
    } TStubMethod;
    """
    _fields_ = [("stubAtEnd", TBoolean),
                ("longStub", TBoolean)]


class TRatePt(ct.Structure):
    """
    lib/include/isda/bastypes.h-28
    typedef struct
    {
        TDate  fDate;
        double fRate;
    } TRatePt;"""
    _fields_ = [("fDate", TDate),
                ("fRate", ct.c_double)]


class TCurve(ct.Structure):
    """
    lib/include/isda/bastypes.h-38
    typedef struct _TCurve
    {
        int       fNumItems;     /* Number of TRatePts in fArray */
        TRatePt  *fArray;        /* Dates & rates */
        TDate     fBaseDate;     /* Discount date */
        double    fBasis;        /* Number compounding periods / year */
        long      fDayCountConv; /* How the year fraction is computed */
    } TCurve;"""
    _fields_ = [("fNumItems", ct.c_int),
                ("fArray", ct.POINTER(TRatePt)),
                ("fBaseDate", ct.c_int),
                ("fBasis", ct.c_double),
                ("fDayCountConv", ct.c_int)]


"""Date utility functions"""
def tdate(dt):
    return dt.toordinal() - tdate_base


def tdate_to_date(tdt):
    return datetime.date.fromordinal(tdate_base + tdt)


"""Equivalents to XLL functions"""
def version_string():
    """
    version.c:25
    EXPORT int JpmcdsVersionString (char *version)
    """
    out = ct.create_string_buffer(100)
    cdsdll.JpmcdsVersionString(out)
    return out.value


def enable_logging(file_name="errors.log"):
    """
    cerror.c:216
    EXPORT char* JpmcdsErrMsgGetFileName(void)
    """
    cdsdll.JpmcdsErrMsgFileName.argtypes = [ct.c_char_p, TBoolean]
    print("Opening file", cdsdll.JpmcdsErrMsgFileName(file_name, 0))
    """Use XLA API because there doesn't seem to be a way to
    enable logging directly through normal DLL interface"""
    """../../excel/src/cdsfuncs.c:387:XLFUNC(LPXLOPER) CDS_SetErrorLogStatus(OPER *a1)"""
    xltypeNum = 0x0001
    op = OPER(num=1, type=xltypeNum)
    cdsdll.CDS_SetErrorLogStatus.restype = ct.c_wchar_p
    cdsdll.CDS_SetErrorLogStatus(ct.byref(op))
    return cdsdll.JpmcdsErrMsgStatus()


def make_tcurve(today, dates, rates, dcc="ACT/360"):
    """
    lib/include/isda/tcurve.h:48
    EXPORT TCurve* JpmcdsMakeTCurve
        (TDate   baseDate,          /* (I) Value date */
         TDate  *dates,             /* (I) Array of dates for curve */
         double *rates,             /* (I) Array of rates for dates of curve */
         int     numPts,            /* (I) # elements in dates & rates arrays */
         double  basis,             /* (I) Basis. See JpmcdsRateToDiscount */
         long    dayCountConv);     /* (I) See JpmcdsDayCountFraction */
    """
    cdsdll.JpmcdsMakeTCurve.argtypes = [TDate,
                                        ct.POINTER(TDate),
                                        ct.POINTER(ct.c_double),
                                        ct.c_uint,
                                        ct.c_double,
                                        ct.c_uint]
    cdsdll.JpmcdsMakeTCurve.restype = ct.POINTER(TCurve)
    assert len(dates) == len(rates)
    n = len(dates)
    dates = (TDate * n)(*dates)
    rates = (ct.c_double * n)(*rates)
    tcurve = cdsdll.JpmcdsMakeTCurve(tdate(today),
                                     dates,
                                     rates,
                                     2,
                                     1.,
                                     jpm_dcc[dcc])
    return tcurve


def build_ir_zero_curve(spot_date, types, end_dates, rates, mm_dcc, fixed_ivl, float_ivl, fixed_dcc, float_dcc,
                        swap_bdc, holidays):

    """
    Corresponds to CDS_IRZeroCurveBuild. Input dates must be bad day adjusted to match that function.

    lib/include/isda/zerocurve.h:28
    EXPORT TCurve* JpmcdsBuildIRZeroCurve(
        TDate      valueDate,      /* (I) Value date                       */
        char      *instrNames,     /* (I) Array of 'M' or 'S'              */
        TDate     *dates,          /* (I) Array of swaps dates             */
        double    *rates,          /* (I) Array of swap rates              */
        long       nInstr,         /* (I) Number of benchmark instruments  */
        long       mmDCC,          /* (I) DCC of MM instruments            */
        long       fixedSwapFreq,  /* (I) Fixed leg freqency               */
        long       floatSwapFreq,  /* (I) Floating leg freqency            */
        long       fixedSwapDCC,   /* (I) DCC of fixed leg                 */
        long       floatSwapDCC,   /* (I) DCC of floating leg              */
        long       badDayConv,     /* (I) Bad day convention               */
        char      *holidayFile);   /* (I) Holiday file                     */
    """

    cdsdll.JpmcdsBuildIRZeroCurve.argtypes = [TDate,
                                              ct.c_char_p,
                                              ct.POINTER(TDate),
                                              ct.POINTER(ct.c_double),
                                              ct.c_int,
                                              ct.c_int,
                                              ct.c_int,
                                              ct.c_int,
                                              ct.c_int,
                                              ct.c_int,
                                              ct.c_char,#Internaly a long, but expects char value of e.g. 'M'
                                              ct.c_char_p
                                              ]
    cdsdll.JpmcdsBuildIRZeroCurve.restype = ct.POINTER(TCurve)
    n = len(types)
    assert len(types) == len(end_dates) == len(rates)

    types = [bytes(t, 'ASCII') for t in types]
    types = (ct.c_char * n)(*types)
    end_dates = [tdate(d) for d in end_dates]
    end_dates = (TDate * n)(*end_dates)
    rates = (ct.c_double * n)(*rates)

    tcurve = cdsdll.JpmcdsBuildIRZeroCurve(tdate(spot_date),
                                           types,
                                           end_dates,
                                           rates,
                                           n,
                                           jpm_dcc[mm_dcc],
                                           jpm_interval_to_freq[fixed_ivl],
                                           jpm_interval_to_freq[float_ivl],
                                           jpm_dcc[fixed_dcc],
                                           jpm_dcc[float_dcc],
                                           bytes(swap_bdc, 'ASCII'),
                                           bytes(holidays, 'ASCII'))
    if tcurve:
        return tcurve
    else:
        raise Exception("Failed to build curve")


def cdsone_spread(today,
                  cashsettle_date,
                  benchmark_st_date,
                  stepin_date,
                  start_date,
                  end_date,
                  coupon_rate,
                  pay_acc_on_default,
                  coupon_interval,
                  stub_at_end,
                  long_stub,
                  payment_dcc,
                  bad_day_convention,
                  holidays,
                  discount_curve,
                  upfront_charge,
                  recovery_rate):
    """

    TODO: Wrong signature - change

    Equivalent of X. stub_at_end/long_stub as two parameters instead of stubType string

    lib/include/isda/cdsone.h:27:
    EXPORT int JpmcdsCdsoneUpfrontCharge
    (TDate           today,
     TDate           valueDate,
     TDate           benchmarkStartDate,  /* start date of benchmark CDS for
                                          ** internal clean spread bootstrapping */
     TDate           stepinDate,
     TDate           startDate,           /* CDS start date, can be in the past */
     TDate           endDate,
     double          couponRate,
     TBoolean        payAccruedOnDefault,
     TDateInterval  *dateInterval,
     TStubMethod    *stubType,
     long            accrueDCC,
     long            badDayConv,
     char           *calendar,
     TCurve         *discCurve,
     double          oneSpread,
     double          recoveryRate,
     TBoolean        payAccruedAtStart,
     double         *upfrontCharge);
    """
    holidays = holidays or "NONE"# 'NONE' being the internal string that represents no calendar
    cdsdll.JpmcdsCdsoneSpread.argtypes = [TDate,#today
                                          TDate,#valueDate
                                          TDate,#benchmarkStartDate,
                                          TDate,#stepinDate,
                                          TDate,#startDate,
                                          TDate,#endDate
                                          ct.c_double,#couponRate
                                          TBoolean,#payAccruedOnDefault
                                          ct.POINTER(TDateInterval),#dateInterval
                                          ct.POINTER(TStubMethod),#stubType
                                          ct.c_uint,#accrueDCC
                                          ct.c_char,#badDayConv
                                          ct.c_char_p,#calendar
                                          ct.c_void_p,#discCurve
                                          ct.c_double,#upfront
                                          ct.c_double,#recoveryRate
                                          TBoolean,#payAccruedAtStart
                                          ct.POINTER(ct.c_double),#oneSpread
                                         ]

    s = ct.c_double(-999.)
    ret = cdsdll.JpmcdsCdsoneSpread(tdate(today),
                                    tdate(cashsettle_date),
                                    tdate(benchmark_st_date),
                                    tdate(stepin_date),
                                    tdate(start_date),
                                    tdate(end_date),
                                    coupon_rate,
                                    pay_acc_on_default,
                                    ct.byref(TDateInterval(1, bytes(coupon_interval, 'ASCII'), 0)),
                                    ct.byref(TStubMethod(stub_at_end, long_stub)),
                                    jpm_dcc[payment_dcc],
                                    bytes(bad_day_convention, 'ASCII'),
                                    bytes(holidays, 'ASCII'),
                                    discount_curve,
                                    upfront_charge,
                                    recovery_rate,
                                    1,#payAccruedAtStart - TODO: what is this?
                                    ct.byref(s))
    return s.value, ret


def test():
    today = datetime.date(2008, 9, 18)

    curve_data = """M	20-Oct-08	 0.00445000
    M	18-Nov-08	 0.00948800
    M	18-Dec-08	 0.01233700
    M	19-Jan-09	 0.01776200
    M	16-Feb-09	 0.01935000
    M	21-Sep-09	 0.02083800
    S	20-Sep-10	 0.01652000
    S	19-Sep-11	 0.02018000
    S	18-Sep-12	 0.02303300
    S	18-Sep-13	 0.02525000
    S	18-Sep-14	 0.02696000
    S	18-Sep-15	 0.02825000
    S	19-Sep-16	 0.02931000
    S	18-Sep-17	 0.03017000
    S	17-Sep-18	 0.03092000
    S	17-Sep-19	 0.03160000
    S	16-Sep-20	 0.03231000
    S	18-Sep-23	 0.03367000
    S	14-Sep-28	 0.03419000
    S	13-Sep-33	 0.03411000
    S	13-Sep-38	 0.03412000"""

    types, end_dates, rates = zip(*[[e.strip() for e in row.split('\t')] for row in curve_data.split("\n")])
    rates = [float(r) for r in rates]
    end_dates = [datetime.datetime.strptime(s, "%d-%b-%y").date() for s in end_dates]
    mm_dcc = "ACT/360"
    fixed_ivl = "6M"
    float_ivl = "3M"
    fixed_dcc = "B30/360"
    float_dcc = "ACT/360"
    swap_bdc = "M"
    holidays = "NONE"
    curve_spot_date = datetime.date(2008, 9, 22)
    curve = build_ir_zero_curve(curve_spot_date, types, end_dates, rates, mm_dcc, fixed_ivl,
                                float_ivl, fixed_dcc, float_dcc, swap_bdc, holidays)

    expected_rates = [0.004521214932339923, 0.009658908310329117, 0.012568060258673963, 0.018118211800616235,
                      0.01973383741757484, 0.020073579775643058, 0.02010783208459177, 0.0201189902820742,
                      0.020130022518219715, 0.020140930913962185, 0.02015171754288536, 0.021109492799824547,
                      0.021114861103672, 0.021117522898129293, 0.021120169952683954, 0.02112802389119728,
                      0.0181548327419927, 0.01814339720325031, 0.01813200406779547, 0.01812065310021649,
                      0.01810934406684117, 0.01807566626096868, 0.01659723057889706, 0.016590846432126938,
                      0.01657813116532636, 0.016571799898685846, 0.016552910694618925, 0.01874631226687984,
                      0.018766498958423528, 0.01877655903185982, 0.01878659702112584, 0.01881657920870694,
                      0.02028669563382679, 0.020293693497408283, 0.020307650812178002, 0.02032849127903432,
                      0.021969541424180505, 0.02197757152187707, 0.02199359399819434, 0.022017533856876215,
                      0.023261671864614364, 0.023267804085979282, 0.023286150429225883, 0.02329224916067062,
                      0.0233002757796823, 0.024554316802869014, 0.02456071483539879, 0.024586229242810065,
                      0.024592588484755096, 0.02561353255945087, 0.02562903607901723, 0.025634192619156115,
                      0.02563934352487025, 0.025645358802366847, 0.026605756547755766, 0.026610763705101848,
                      0.026625755310228705, 0.026630742581031264, 0.026635724898709245, 0.02745854950324378,
                      0.02746274376841451, 0.027466934213295602, 0.027471120843103707, 0.027475575100552874,
                      0.028197267023141848, 0.028208712199929487, 0.028212520840031008, 0.02821632627746551,
                      0.028220128516271092, 0.02885284490800233, 0.028859414613341183, 0.02886269561869992,
                      0.028865974063188457, 0.028876691000073285, 0.029461253694610612, 0.02946750337637649,
                      0.029470624801344547, 0.029473743952354026, 0.02948308778645292, 0.029997705882209624,
                      0.030000452933262478, 0.030005941404941616, 0.030014160065215033, 0.030498706038314616,
                      0.030501318037612135, 0.030506536995657196, 0.030509143957646145, 0.030516954794882478,
                      0.030952582495314518, 0.030954910558872006, 0.030964208682991788, 0.03139165048610071,
                      0.03139395537291234, 0.03139856116736106, 0.031405459928257606, 0.03179452396113547,
                      0.031796602739957436, 0.031802832261541215, 0.03180515766480441, 0.03219555915165251,
                      0.03219767509859639, 0.03220612787742905, 0.03256718340250098, 0.0325729610637997,
                      0.03257488503794765, 0.03257733471323454, 0.03299259128681742, 0.03299932876736933,
                      0.03300381509186301, 0.03339086082871101, 0.033394983297786854, 0.03339860192748478,
                      0.03370798562863486, 0.03371299374990677, 0.03400037626882657, 0.03400192100565502,
                      0.034011175804718574, 0.03427121290575719, 0.03427694132191639, 0.03452412705140939,
                      0.03452545944649188, 0.03453211362530961, 0.03475710293544587, 0.034758345918067146,
                      0.0347657939801318, 0.034978182318675666, 0.034979343325356016, 0.03498398312318516,
                      0.035040365662991846, 0.035040674184824594, 0.035095354747565644, 0.035096222613929084,
                      0.03514616799101922, 0.03514644024228519, 0.03519529737859406, 0.03523987533720785,
                      0.03524060117033723, 0.03528339527206703, 0.03532388260670061, 0.03536224479465022,
                      0.03536245001444294, 0.03539864487771216, 0.035398839736092036, 0.035433600109306296,
                      0.03543378527014202, 0.035395863157532625, 0.03535894151367458, 0.03532431352098353,
                      0.03529071357631142, 0.03525913241854317, 0.03522809888590972, 0.035199036761452085,
                      0.035171183420658725, 0.03514446498439239, 0.03511867536067997, 0.03510631668179731,
                      0.03509423667060396, 0.03508280237186012, 0.03507160931954889, 0.03506094188326858,
                      0.03505043265855856, 0.035040674024525265, 0.035030935241209926, 0.03502168106892167,
                      0.03501273465877146]
    actual_rates = [x.fRate for x in curve.contents.fArray[0:curve.contents.fNumItems]]
    matches = True
    for expected, actual in zip(expected_rates,actual_rates):
        matches = matches and abs(expected - actual) < 1e-25
    if matches:
        print("IR curve matches", actual)
    else:
        raise Exception("IR curve doesn't match!")

    effective_date = datetime.date(2007, 3, 20)
    maturity = datetime.date(2013, 6, 20)
    settle = datetime.date(2008, 9, 23)

    spread, ret = cdsone_spread(today,
                                settle,
                                effective_date,
                                today + datetime.timedelta(days=1),
                                effective_date,
                                maturity,
                                0.05,
                                1,
                                 "Q",
                                False, False,
                                "ACT/360",
                                "F",
                                "NONE",
                                curve,
                                0.0185819118738166,
                                0.40
                                )
    if abs(spread - 0.055) < 1e-15:
        print("Spread matches", spread)
    else:
        raise Exception("Spread doesn't match!")
